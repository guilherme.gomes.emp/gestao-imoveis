FROM node:16.13.1 as base

WORKDIR /usr/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

COPY . .

EXPOSE 3000

RUN yarn
CMD [ "yarn", "start"]