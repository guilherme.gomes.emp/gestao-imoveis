import React, { useContext } from "react";
import "./styles/App.scss";
import AllRoutes from "./routes/Routes";
import Sidebar from "./components/sidebar/Sidebar";
import Topbar from "./components/topbar/Topbar";
import Login from "./pages/login/Login";
import { AuthContext } from "./contexts/Auth";

function App() {
  const { authenticated } = useContext(AuthContext);
  return authenticated === true ? (
    <div>
      <Topbar />
      <Sidebar>
        <AllRoutes />
      </Sidebar>
    </div>
  ) : (
    <Login/>
  );
}

export default App;
