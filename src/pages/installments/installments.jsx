import React, { useState, useCallback, useEffect } from "react";
import * as MdIcons from "react-icons/md";
import { api } from "../../services/api";
import "./Installments.scss";

function Installments() {
  const [valor, setValor] = useState("");
  const [data_vencimento, setVencimento] = useState("");
  const [data_pagamento, setPagamento] = useState("");
  const [nome_locatario, setLocatario] = useState("");
  const [situacao, setSitucao] = useState("NAO_PAGO");

  const [responseData, setResponseData] = useState([]);

  function ChoiceItems(i) {
    return <option>{i.nome_locatario}</option>;
  }

  function refreshPage() {
    window.location.reload(true);
  }

  async function onSubmit() {
    await api({
      method: "post",
      url: "/contrato/parcel",
      data: {
        valor: valor,
        data_vencimento: data_vencimento,
        data_pagamento: data_pagamento,
        nome_locador: nome_locatario,
        situacao: situacao,
      },
    })
      .then((res) => {
        console.log(res.status);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const fetchData = useCallback(() => {
    api({
      method: "GET",
      url: "/get_all_locatarios",
    })
      .then((response) => {
        setResponseData(response.data.entries);
      })
      .catch((error) => {
        console.log(error);
      });
  });

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="installment">
      <select className="botao_inst" onChange={(e) => setLocatario(e.target.value)}>
        <option>Escolha um locatário</option>
        {responseData.map(ChoiceItems)}
      </select>
      <div className="paper">
        <div className="itens-lista">
          <div className="container">
            <label>
              Data de vencimento
              <input
                type="date"
                className="input"
                onChange={(e) => setVencimento(e.target.value)}
              ></input>
            </label>
            <label>
              Data do pagamento
              <input
                type="date"
                className="input"
                placeholder="2019-05-24"
                onChange={(e) => setPagamento(e.target.value)}
              ></input>
            </label>
            <label>
              Valor do pagamento
              <input
                type="number"
                className="input"
                placeholder="100000"
                onChange={(e) => setValor(e.target.value)}
              ></input>
            </label>
            <label>
              Situação
              <select className="input" onChange={(e) => setSitucao(e.target.value)} defaultValue={"NAO_PAGO"}>
                <option>PAGO</option>
                <option>NAO_PAGO</option>
              </select>
            </label>
            <button className="button_add" onClick={() => [onSubmit(), refreshPage()]}>
              <MdIcons.MdAddCircle size={30} />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Installments;
