import React, { useState, useCallback, useEffect } from "react";
import "./Reports.scss";
import { api } from "../../services/api";
import fileDownload from "js-file-download";
import * as FaIcons from "react-icons/fa";

function Reports() {
  const [proprietarios, setProprietarios] = useState([]);
  const [responseData, setResponseData] = useState([]);
  const [responseDataLt, setResponseDataLt] = useState([]);
  const [propNome, setPropNome] = useState("");
  const [text, setText] = useState("");

  function ChoiceItem(i) {
    return <option>{i.nome_locador}</option>;
  }

  function ChoiceItems(i) {
    return <option>{i.nome_locatario}</option>;
  }

  const fetchData = useCallback(() => {
    api({
      method: "GET",
      url: "/get_locadores",
    })
      .then((response) => {
        setResponseData(response.data.entries);
      })
      .catch((error) => {
        console.log(error);
      });
  });

  useEffect(() => {
    fetchData();
  }, []);

  const searchLt = useCallback(() => {
    api({
      method: "GET",
      url: "/get_all_locatarios",
    })
      .then((response) => {
        setResponseDataLt(response.data.entries);
      })
      .catch((error) => {
        console.log(error);
      });
  });

  useEffect(() => {
    searchLt();
  }, []);

  const addItem = (item) => {
    setProprietarios((prev) => [...prev, item]);

    console.log(proprietarios);
  };

  const downloadReports = (e) => {
    e.preventDefault();
    api({
      url: "/download_relatorio_imoveis",
      method: "get",
      params: { proprietario: propNome },
      responseType: "blob",
    })
      .then((res) => {
        console.log(res.data);
        fileDownload(res.data, "imoveis.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const downloadPortion = (e) => {
    e.preventDefault();
    api({
      url: "/download_relatorio_parcelas",
      method: "get",
      params: { proprietarios: proprietarios },
      responseType: "blob",
    })
      .then((res) => {
        console.log(res.data);
        fileDownload(res.data, "parcelas.xlsx");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="reports">
      <select
        className="select-locador"
        onChange={(e) => setPropNome(e.target.value)}
      >
        <option>Escolha um locador</option>
        {responseData.map(ChoiceItem)}
      </select>
      <select
        className="select-locador"
        onChange={(e) => setText(e.target.value)}
      >
        <option>Escolha um locatário</option>
        {responseDataLt.map(ChoiceItems)}
      </select>
      <button className="botao-adicionar" onClick={() => addItem(text)}>
        <i>
          <FaIcons.FaPlus size={15} />
        </i>
      </button>
      <div className="estilo-botao">
        <button className="botao-relatorio" onClick={(e) => downloadReports(e)}>
          Relatório de imóveis
          <i className="icon-download">
            <FaIcons.FaDownload size={15} />
          </i>
        </button>
      </div>
      <div className="estilo-botao">
        <button className="botao-relatorio" onClick={(e) => downloadPortion(e)}>
          Relatório de parcelas
          <i className="icon-download">
            <FaIcons.FaDownload size={15} />
          </i>
        </button>
      </div>
    </div>
  );
}

export default Reports;
