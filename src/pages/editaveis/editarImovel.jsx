import React, { useState, useCallback, useEffect } from "react";
import "./editarGeral.scss";
import { useParams } from "react-router-dom";
import { api } from "../../services/api";
import { NavLink } from "react-router-dom"

function EditFormProperty() {
  const { id } = useParams();

  const listaEstados = [
    "Escolha um Estado",
    "GO",
    "SP",
    "DF",
    "MG",
    "MT",
    "RO",
    "AC",
    "AM",
    "RR",
    "PA",
    "AP",
    "TO",
    "MA",
    "PI",
    "CE",
    "RN",
    "PB",
    "PE",
    "AL",
    "SE",
    "BA",
    "ES",
    "RJ",
    "PR",
    "SC",
    "RS",
    "MS",
  ];

  const [descricao, setDescricao] = useState("");
  const [iptu, setIPTU] = useState("");
  const [nome_cartorio, setNomeCartorio] = useState("");
  const [registro, setRegistro] = useState("");
  const [medida, setMedida] = useState("");
  const [cep, setCEP] = useState("");
  const [rua, setRua] = useState("");
  const [cidade, setCidade] = useState("");
  const [uf, setUf] = useState("");
  const [bairro, setBairro] = useState("");
  const [adicional, setAdicional] = useState("");
  const [locador, setLocador] = useState("");

  const [responseData, setResponseData] = useState([]);
  const [responseDataValue, setResponseDataValue] = useState("");

  function ChoiceItem(i) {
    return <option>{i}</option>;
  }

  function ChoiceItems(i) {
    return <option>{i.nome_locador}</option>;
  }

  const gtData = useCallback(() => {
    api({
      method: "get",
      url: `/get_imovel/${id}`,
    })
      .then((response) => {
        setResponseDataValue(response.data);
        setDescricao(response.data.descricao);
        setIPTU(response.data.iptu);
        setNomeCartorio(response.data.nome_cartorio);
        setRegistro(response.data.numero_registro);
        setMedida(response.data.medida);
        setCEP(response.data.cep);
        setRua(response.data.rua);
        setCidade(response.data.cidade);
        setUf(response.data.uf);
        setBairro(response.data.bairro);
        setAdicional(response.data.adicional);
        setLocador(response.data.locador);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    gtData();
  }, [gtData]);

  async function onSubmit() {
    await api({
      method: "patch",
      url: `/patch_imovel/${id}`,
      data: {
        descricao: descricao,
        iptu: iptu,
        nome_cartorio: nome_cartorio,
        numero_registro: registro,
        medida: medida,
        cep: cep,
        rua: rua,
        cidade: cidade,
        uf: uf,
        bairro: bairro,
        adicional: adicional,
        locador_cadastrado: locador,
      },
    })
      .then((res) => {
        console.log(res.status);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const fetchData = useCallback(() => {
    api({
      method: "GET",
      url: "/get_locadores",
    })
      .then((response) => {
        setResponseData(response.data.entries);
      })
      .catch((error) => {
        console.log(error);
      });
  });
  useEffect(() => {
    fetchData();
  }, []);


  return (
    <div className="edit">
      <form className="formulario_pro" onSubmit={(e) => e.preventDefault()}>
        <input
          className="input"
          placeholder="Descrição"
          defaultValue={responseDataValue.descricao}
          onChange={(e) => setDescricao(e.target.value)}
        />
        <input
          className="input"
          placeholder="IPTU"
          defaultValue={responseDataValue.iptu}
          onChange={(e) => setIPTU(e.target.value)}
        />
        <input
          className="input"
          placeholder="Nome do cartório"
          defaultValue={responseDataValue.nome_cartorio}
          onChange={(e) => setNomeCartorio(e.target.value)}
        />
        <input
          className="input"
          placeholder="Número do resgistro"
          defaultValue={responseDataValue.numero_registro}
          onChange={(e) => setRegistro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Medida"
          defaultValue={responseDataValue.medida}
          onChange={(e) => setMedida(e.target.value)}
        />
        <select className="input" onChange={(e) => setUf(e.target.value)}>
          {listaEstados.map(ChoiceItem)}
        </select>
        <input
          className="input"
          placeholder="CEP"
          defaultValue={responseDataValue.cep}
          onChange={(e) => setCEP(e.target.value)}
        />
        <input
          className="input"
          placeholder="Rua"
          defaultValue={responseDataValue.rua}
          onChange={(e) => setRua(e.target.value)}
        />
        <input
          className="input"
          placeholder="Cidade"
          defaultValue={responseDataValue.cidade}
          onChange={(e) => setCidade(e.target.value)}
        />
        <input
          className="input"
          placeholder="Bairro"
          defaultValue={responseDataValue.bairro}
          onChange={(e) => setBairro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Adicional (Lote/Quadra/N°)"
          defaultValue={responseDataValue.adicional}
          onChange={(e) => setAdicional(e.target.value)}
        />
        <select className="input" onChange={(e) => setLocador(e.target.value)}>
          <option>Escolha um locador</option>
          {responseData.map(ChoiceItems)}
        </select>

        <NavLink to="/property">
          <button className="botao_p" type="button" onClick={() => onSubmit()}>
            Salvar
          </button>
        </NavLink>
        <NavLink to="/property">
          <button className="botao_c" type="button">
            Cancelar
          </button>
        </NavLink>
      </form>
    </div>
  );
}

export default EditFormProperty;
