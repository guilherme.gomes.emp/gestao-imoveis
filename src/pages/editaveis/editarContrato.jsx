import React, { useState, useCallback, useEffect } from "react";
import { useParams } from "react-router-dom";
import { api } from "../../services/api";
import "./editarGeral.scss";
import { NavLink } from "react-router-dom";

function EditFormAgreement(props) {
  const { id } = useParams();
  const listaEstados = [
    "Escolha um Estado",
    "GO",
    "SP",
    "DF",
    "MG",
    "MT",
    "RO",
    "AC",
    "AM",
    "RR",
    "PA",
    "AP",
    "TO",
    "MA",
    "PI",
    "CE",
    "RN",
    "PB",
    "PE",
    "AL",
    "SE",
    "BA",
    "ES",
    "RJ",
    "PR",
    "SC",
    "RS",
    "MS",
  ];

  const [nome, setNome] = useState("");
  const [documento, setDocumento] = useState("");
  const [uf, setUf] = useState("");
  const [cidade, setCidade] = useState("");
  const [bairro, setBairro] = useState("");
  const [cep, setCEP] = useState("");
  const [rua, setRua] = useState("");
  const [adicional, setAdicional] = useState("");
  const [telefone, setTelefone] = useState("");
  const [email, setEmail] = useState("");
  const [aluguel, setAluguel] = useState("");
  const [imovel, setImovel] = useState("");
  const [data_inicial, setDataInicial] = useState("")
  const [data_final, setDataFinal] = useState("")

  const [responseData, setResponseData] = useState([]);
  const [responseDataValue, setResponseDataValue] = useState("");

  function ChoiceItem(i) {
    return <option>{i}</option>;
  }

  function ChoiceItems(i) {
    return <option>{i.descricao}</option>;
  }

  const gtData = useCallback(() => {
    api({
      method: "get",
      url: `/get_contrato/${id}`,
    })
      .then((response) => {
        setResponseDataValue(response.data);
        setNome(response.data.nome)
        setDocumento(response.data.documento)
        setCidade(response.data.cidade)
        setCEP(response.data.documento)
        setRua(response.data.rua)
        setUf(response.data.uf)
        setBairro(response.data.bairro)
        setAdicional(response.data.adicional)
        setTelefone(response.data.telefone)
        setEmail(response.data.email)
        setAluguel(response.data.valor_aluguel)
        setImovel(response.data.imovel_cadastrado)
        setDataInicial(response.data.data_inicial)
        setDataFinal(response.data.data_final)
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    gtData();
  }, [gtData]);

  async function onSubmit() {
    await api({
      method: "patch",
      url: `/patch_contrato/${id}`,
      data: {
        nome_locatario: nome,
        documento: documento,
        cep: cep,
        rua: rua,
        cidade: cidade,
        uf: uf,
        bairro: bairro,
        adicional: adicional,
        telefone: telefone,
        email: email,
        valor_aluguel: aluguel,
        imovel_cadastrado: imovel,
        data_inicial: data_inicial,
        data_final: data_final
      },
    })
      .then((res) => {
        console.log(res.status);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const fetchData = useCallback(() => {
    api({
      method: "GET",
      url: "/get_imoveis",
    })
      .then((response) => {
        setResponseData(response.data.entries);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="edit">
      <form className="formulario_agree" onSubmit={(e) => e.preventDefault()}>
        <input
          className="input"
          placeholder="Nome do locatário"
          defaultValue={responseDataValue.nome_locatario}
          onChange={(e) => setNome(e.target.value)}
        />
        <input
          className="input"
          placeholder="CPF ou CNPJ"
          defaultValue={responseDataValue.documento}
          onChange={(e) => setDocumento(e.target.value)}
        />
        <select
          className="input"
          defautvalue={uf}
          onChange={(e) => setUf(e.target.value)}
        >
          {listaEstados.map(ChoiceItem)}
        </select>
        <input
          className="input"
          placeholder="CEP"
          defaultValue={responseDataValue.cep}
          onChange={(e) => setCEP(e.target.value)}
        />
        <input
          className="input"
          placeholder="Rua"
          defaultValue={responseDataValue.rua}
          onChange={(e) => setRua(e.target.value)}
        />
        <input
          className="input"
          placeholder="Cidade"
          defaultValue={responseDataValue.cidade}
          onChange={(e) => setCidade(e.target.value)}
        />
        <input
          className="input"
          placeholder="Bairro"
          defaultValue={responseDataValue.bairro}
          onChange={(e) => setBairro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Adicional (Lote/Quadra/N°)"
          defaultValue={responseDataValue.adicional}
          onChange={(e) => setAdicional(e.target.value)}
        />
        <input
          className="input"
          placeholder="Telefone"
          defaultValue={responseDataValue.telefone}
          onChange={(e) => setTelefone(e.target.value)}
        />
        <input
          className="input"
          placeholder="E-mail"
          defaultValue={responseDataValue.email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          className="input"
          placeholder="Valor do aluguel"
          defaultValue={responseDataValue.valor_aluguel}
          onChange={(e) => setAluguel(e.target.value)}
        />
        <select
          className="input"
          onChange={(e) => setImovel(e.target.value)}
        >
          <option>Escolha um imóvel</option>
          {responseData.map(ChoiceItems)}
        </select>
        <input
          className="input"
          type="date"
          defaultValue={responseDataValue.data_inicial}
          onChange={(e) => setDataInicial(e.target.value)}
        />
        <input
          className="input"
          type="date"
          defaultValue={responseDataValue.data_final}
          onChange={(e) => setDataFinal(e.target.value)}
        />
        <NavLink to="/agreement">
          <button className="botao_p" type="button" onClick={() => onSubmit()}>
            Salvar
          </button>
        </NavLink>
        <NavLink to="/agreement">
          <button className="botao_c" type="button">
            Cancelar
          </button>
        </NavLink>
      </form>
    </div>
  );
}

export default EditFormAgreement;
