import React, { useState, useCallback, useEffect } from "react";
import "./editarGeral.scss";
import { useParams } from "react-router-dom";
import { api } from "../../services/api";
import { NavLink } from "react-router-dom"

function EditFormLocator(props) {
  const { id } = useParams();

  const listaEstados = [
    "Escolha um Estado",
    "GO",
    "SP",
    "DF",
    "MG",
    "MT",
    "RO",
    "AC",
    "AM",
    "RR",
    "PA",
    "AP",
    "TO",
    "MA",
    "PI",
    "CE",
    "RN",
    "PB",
    "PE",
    "AL",
    "SE",
    "BA",
    "ES",
    "RJ",
    "PR",
    "SC",
    "RS",
    "MS",
  ];

  const [nome, setNome] = useState("");
  const [telefone, setTelefone] = useState("");
  const [email, setEmail] = useState("");
  const [documento, setDocumento] = useState("");
  const [uf, setUf] = useState("");
  const [cep, setCEP] = useState("");
  const [rua, setRua] = useState("");
  const [cidade, setCidade] = useState("");
  const [bairro, setBairro] = useState("");
  const [adicional, setAdicional] = useState("");

  const [responseDataValue, setResponseDataValue] = useState("");

  function ChoiceItem(i) {
    return <option>{i}</option>;
  }

  const gtData = useCallback(() => {
    api({
      method: "get",
      url: `/get_locador/${id}`,
    })
      .then((response) => {
        setResponseDataValue(response.data);
        setNome(response.data.nome_locador);
        setTelefone(response.data.telefone);
        setEmail(response.data.email);
        setDocumento(response.data.documento);
        setCEP(response.data.cep);
        setRua(response.data.rua);
        setUf(response.data.uf);
        setCidade(response.data.cidade);
        setBairro(response.data.bairro);
        setAdicional(response.data.adicional);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  useEffect(() => {
    gtData();
  }, [gtData]);

  async function onSubmit() {
    await api({
      method: "patch",
      url: `/patch_locador/${id}`,
      data: {
        nome_locador: nome,
        telefone: telefone,
        email: email,
        documento: documento,
        cep: cep,
        rua: rua,
        cidade: cidade,
        uf: uf,
        bairro: bairro,
        adicional: adicional,
      },
    })
      .then((res) => {
        console.log(res.status);
        props.visible(false);
        // responseCode = res.status
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <div className="edit">
      <form className="formulario_loc" onSubmit={(e) => e.preventDefault()}>
        <input
          className="input"
          placeholder="Nome"
          defaultValue={responseDataValue.nome_locador}
          onChange={(e) => setNome(e.target.value)}
        />
        <input
          className="input"
          placeholder="Telefone"
          defaultValue={responseDataValue.telefone}
          onChange={(e) => setTelefone(e.target.value)}
        />
        <input
          className="input"
          placeholder="E-mail"
          defaultValue={responseDataValue.email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          className="input"
          placeholder="CPF ou CNPJ"
          defaultValue={responseDataValue.documento}
          onChange={(e) => setDocumento(e.target.value)}
        />
        <select className="input" onChange={(e) => setUf(e.target.value)}>
          {listaEstados.map(ChoiceItem)}
        </select>
        <input
          className="input"
          placeholder="CEP"
          defaultValue={responseDataValue.cep}
          onChange={(e) => setCEP(e.target.value)}
        />
        <input
          className="input"
          placeholder="Rua"
          defaultValue={responseDataValue.rua}
          onChange={(e) => setRua(e.target.value)}
        />
        <input
          className="input"
          placeholder="Cidade"
          defaultValue={responseDataValue.cidade}
          onChange={(e) => setCidade(e.target.value)}
        />
        <input
          className="input"
          placeholder="Bairro"
          defaultValue={responseDataValue.bairro}
          onChange={(e) => setBairro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Adicional (Lote/Quadra/N°)"
          defaultValue={responseDataValue.adicional}
          onChange={(e) => setAdicional(e.target.value)}
        />
               <NavLink to="/locator">
          <button className="botao_p" type="button" onClick={() => onSubmit()}>
            Salvar
          </button>
        </NavLink>
        <NavLink to="/locator">
          <button className="botao_c" type="button">
            Cancelar
          </button>
        </NavLink>
      </form>
    </div>
  );
}

export default EditFormLocator;
