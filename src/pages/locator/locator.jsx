import React, { useState } from 'react'
import Modal from '../../components/modal/Modal'
import FormLocator from './components/form/FormLocator'
import Paper from "./components/paper/Paper";
import './styles/Locator.scss'

function Locator() {
  const [isModalVisible, setModalVisible] = useState(false)
  return (
    <div id="locador">
      <button id="botao_loc" onClick={() => setModalVisible(true)}>Novo locador +</button>
      {isModalVisible ? (
         <Modal onClose={() => setModalVisible(false)}>
         <FormLocator visible={setModalVisible}/>
      </Modal>
      ) : null}
      <Paper></Paper>
    </div>
  )
}

export default Locator
