import React, { useState } from "react";
import "../../styles/FormLocator.scss";
import { api } from "../../../../services/api";

function FormLocator(props) {
  const listaEstados = [
    "Escolha um Estado",
    "GO",
    "SP",
    "DF",
    "MG",
    "MT",
    "RO",
    "AC",
    "AM",
    "RR",
    "PA",
    "AP",
    "TO",
    "MA",
    "PI",
    "CE",
    "RN",
    "PB",
    "PE",
    "AL",
    "SE",
    "BA",
    "ES",
    "RJ",
    "PR",
    "SC",
    "RS",
    "MS",
  ];

  const [nome, setNome] = useState("");
  const [telefone, setTelefone] = useState("");
  const [email, setEmail] = useState("");
  const [documento, setDocumento] = useState("");
  const [uf, setUf] = useState("");
  const [cep, setCEP] = useState("");
  const [rua, setRua] = useState("");
  const [cidade, setCidade] = useState("");
  const [bairro, setBairro] = useState("");
  const [adicional, setAdicional] = useState("");

  function ChoiceItem(i) {
    return <option>{i}</option>;
  }

  function refreshPage() {
    window.location.reload(true);
  }

  // let responseCode = 0

  async function onSubmit() {
    await api({
      method: "post",
      url: "/locador",
      data: {
        nome_locador: nome,
        telefone: telefone,
        email: email,
        documento: documento,
        cep: cep,
        rua: rua,
        cidade: cidade,
        uf: uf,
        bairro: bairro,
        adicional: adicional,
      },
    })
      .then((res) => {
        console.log(res.status);
        props.visible(false);
        // responseCode = res.status
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <div className="locador">
      <form className="formulario_loc" onSubmit={(e) => e.preventDefault()}>
        <input
          className="input"
          placeholder="Nome"
          onChange={(e) => setNome(e.target.value)}
        />
        <input
          className="input"
          placeholder="Telefone"
          onChange={(e) => setTelefone(e.target.value)}
        />
        <input
          className="input"
          placeholder="E-mail"
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          className="input"
          placeholder="CPF ou CNPJ"
          onChange={(e) => setDocumento(e.target.value)}
        />
        <select className="input" onChange={(e) => setUf(e.target.value)}>
          {listaEstados.map(ChoiceItem)}
        </select>
        <input
          className="input"
          placeholder="CEP"
          onChange={(e) => setCEP(e.target.value)}
        />
        <input
          className="input"
          placeholder="Rua"
          onChange={(e) => setRua(e.target.value)}
        />
        <input
          className="input"
          placeholder="Cidade"
          onChange={(e) => setCidade(e.target.value)}
        />
        <input
          className="input"
          placeholder="Bairro"
          onChange={(e) => setBairro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Adicional (Lote/Quadra/N°)"
          onChange={(e) => setAdicional(e.target.value)}
        />
        <button
          className="botao_l"
          type="button"
          onClick={() => [onSubmit(), refreshPage()]}
        >
          Cadastrar
        </button>
      </form>
      {/* {responseCode == 200 ? () : ()} */}
    </div>
  );
}

export default FormLocator;
