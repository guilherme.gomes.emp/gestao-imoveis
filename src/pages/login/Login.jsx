import React, { useEffect, useState, useContext } from "react";
import "./Login.scss";
import { AuthContext } from "../../contexts/Auth"

function Login() {
  const { login } = useContext(AuthContext)

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    // console.log("submit", { email, password })
    login(email, password) // Integração com o meu contexto / api
  };

  return (
    <>
      <div className="login">
        <div className="login-page">
          <div className="form">
            <input
              type="text"
              placeholder="E-mail"
              className="e-mail"
              maxLength="40"
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              className="password"
              maxLength="20"
              onChange={(e) => setPassword(e.target.value)}
            />
            <button onClick={handleSubmit}>Entrar</button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;
