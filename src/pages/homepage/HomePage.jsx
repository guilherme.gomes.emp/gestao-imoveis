import React, { useCallback, useEffect, useState } from "react";
import { api } from "../../services/api";
import Topbar from "../../components/topbar/Topbar";
import "./HomePage.scss";

function HomePage() {
  const [responseData, setResponseData] = useState("");

  const fetchData = useCallback(() => {
    api({
      method: "get",
      url: "/get_relatorio",
    })
      .then((response) => {
        setResponseData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  return (
    <>
    <div id="container-page">
      <div className="home-page">
        <div className="box">
          <div className="box-text">Contratos ativos 
          <div className="box-dados">
              <p>{responseData.countContratos}</p>
            </div>
          </div>
        </div>
        <div className="box">
          <div className="box-text">Imóveis ativos
          <div className="box-dados">
              <p>{responseData.countImoveis}</p>
            </div>
          </div>
        </div>
        <div className="box">
          <div className="box-text"> Locadores ativos
          <div className="box-dados">
              <p>{responseData.countLocadores}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    </>
  );
}

export default HomePage;
