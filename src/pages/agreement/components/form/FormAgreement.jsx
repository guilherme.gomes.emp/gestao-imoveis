import React, { useState, useCallback, useEffect } from "react";
import "../../styles/FormAgreement.scss";
import { api } from "../../../../services/api";

function FormAgreement(props) {
  const listaEstados = [
    "Escolha um Estado",
    "GO",
    "SP",
    "DF",
    "MG",
    "MT",
    "RO",
    "AC",
    "AM",
    "RR",
    "PA",
    "AP",
    "TO",
    "MA",
    "PI",
    "CE",
    "RN",
    "PB",
    "PE",
    "AL",
    "SE",
    "BA",
    "ES",
    "RJ",
    "PR",
    "SC",
    "RS",
    "MS",
  ];

  const [nome, setNome] = useState("");
  const [documento, setDocumento] = useState("");
  const [uf, setUf] = useState("");
  const [cidade, setCidade] = useState("");
  const [bairro, setBairro] = useState("");
  const [cep, setCEP] = useState("");
  const [rua, setRua] = useState("");
  const [adicional, setAdicional] = useState("");
  const [telefone, setTelefone] = useState("");
  const [email, setEmail] = useState("");
  const [aluguel, setAluguel] = useState("");
  const [imovel, setImovel] = useState("");
  const [data_inicial, setDataInicial] = useState("");
  const [data_final, setDataFinal] = useState("");

  const [responseData, setResponseData] = useState([]);

  function ChoiceItem(i) {
    return <option>{i}</option>;
  }

  function ChoiceItems(i) {
    return <option>{i.descricao}</option>;
  }

  function refreshPage() {
    window.location.reload(true);
  }

  async function onSubmit() {
    await api({
      method: "post",
      url: "/contrato",
      data: {
        nome_locatario: nome,
        documento: documento,
        cep: cep,
        rua: rua,
        cidade: cidade,
        uf: uf,
        bairro: bairro,
        adicional: adicional,
        telefone: telefone,
        email: email,
        valor_aluguel: aluguel,
        imovel_cadastrado: imovel,
        data_inicial: data_inicial,
        data_final: data_final,
      },
    })
      .then((res) => {
        console.log(res.status);
        props.visible(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const fetchData = useCallback(() => {
    api({
      method: "GET",
      url: "/get_imoveis",
    })
      .then((response) => {
        setResponseData(response.data.entries);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="contrato">
      <form className="formulario_agree" onSubmit={(e) => e.preventDefault()}>
        <input
          className="input"
          placeholder="Nome do locatário"
          onChange={(e) => setNome(e.target.value)}
        />
        <input
          className="input"
          placeholder="CPF ou CNPJ"
          onChange={(e) => setDocumento(e.target.value)}
        />
        <select className="input" onChange={(e) => setUf(e.target.value)}>
          {listaEstados.map(ChoiceItem)}
        </select>
        <input
          className="input"
          placeholder="CEP"
          onChange={(e) => setCEP(e.target.value)}
        />
        <input
          className="input"
          placeholder="Rua"
          onChange={(e) => setRua(e.target.value)}
        />
        <input
          className="input"
          placeholder="Cidade"
          onChange={(e) => setCidade(e.target.value)}
        />
        <input
          className="input"
          placeholder="Bairro"
          onChange={(e) => setBairro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Adicional (Lote/Quadra/N°)"
          onChange={(e) => setAdicional(e.target.value)}
        />
        <input
          className="input"
          placeholder="Telefone"
          onChange={(e) => setTelefone(e.target.value)}
        />
        <input
          className="input"
          placeholder="E-mail"
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          className="input"
          placeholder="Valor do aluguel"
          onChange={(e) => setAluguel(e.target.value)}
        />
        <select
          className="input"
          onChange={(e) => setImovel(e.target.value)}
        >
          <option>Escolha um imóvel</option>
          {responseData.map(ChoiceItems)}
        </select>
        <input
          type="date"
          className="input"
          onChange={(e) => setDataInicial(e.target.value)}
        ></input>
        <input
          type="date"
          className="input"
          onChange={(e) => setDataFinal(e.target.value)}
        ></input>
        <button
          className="botao_a"
          type="button"
          onClick={() => [onSubmit(), refreshPage()]}
        >
          Cadastrar
        </button>
      </form>
    </div>
  );
}

export default FormAgreement;
