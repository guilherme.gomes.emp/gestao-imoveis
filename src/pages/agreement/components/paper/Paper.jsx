import { api } from "../../../../services/api";
import React, { useEffect, useState } from "react";
import Modal from "../../../../components/modal/Modal";
import ListItem from "../listItem/listItem";
import "./Paper.scss";

import FormualrioEdicaoImovel from "../../../editaveis/editarContrato";

function Paper() {
  // const { id } = useParams();
  const [viewEdit, setViewEdit] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    api.get("/get_contratos").then((res) => {
      setData(res.data.entries);
    });
  }, []);

  return (
    <>
      <div className="paper">
        <div className="itens-lista">
          {data.map((item) => {
            return (
              <>
                <div key={item.contrato_id}>
                  <ListItem
                    nome={item.nome_locatario}
                    id={item.contrato_id}
                    desc={item.nome_locatario}
                    toggleEdit={setViewEdit}
                    aluguel={item.valor_aluguel}
                  ></ListItem>
                </div>
              </>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default Paper;
