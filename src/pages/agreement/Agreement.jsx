import React, { useState } from "react";
import Modal from "../../components/modal/Modal";
import FormAgreement from "./components/form/FormAgreement";
import Paper from "./components/paper/Paper";
import "./styles/Agreement.scss";

function Agreement() {
  const [isModalVisible, setModalVisible] = useState(false);
  return (
    <div id="agreement">
      <button id="botao_agree" onClick={() => setModalVisible(true)}>
        Novo contrato +  (OBS: Não pode ter campos vazios)
      </button>
      {isModalVisible ? (
        <Modal onClose={() => setModalVisible(false)}>
          <FormAgreement visible={setModalVisible} />
        </Modal>
      ) : null}
      <Paper></Paper>
    </div>
  );
}

export default Agreement;
