import React, { useState } from "react";
import Modal from "../../components/modal/Modal";
import FormProperty from "./components/form/FormProperty";
import Paper from "./components/paper/Paper";
import "./styles/Property.scss";

function Property() {
  const [isModalVisible, setModalVisible] = useState(false);
  return (
    <div id="property">
      <button id="botao_pro" onClick={() => setModalVisible(true)}>
        Novo imóvel +
      </button>
      {isModalVisible ? (
        <Modal onClose={() => setModalVisible(false)}>
          <FormProperty visible={setModalVisible} />
        </Modal>
      ) : null}
      {<Paper></Paper>}
    </div>
  );
}

export default Property;
