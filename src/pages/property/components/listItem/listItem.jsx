import { api } from "../../../../services/api";
import React, { useState } from "react";
import * as BsIcons from "react-icons/bs";
import "./listItem.scss";
import { useNavigate } from "react-router-dom";

const ListItem = (props) => {
  const navigate = useNavigate()

  function refreshPage() {
    window.location.reload(true);
  }

  const deleteItem = (id) => {
    api({
      url: `/delete_imovel/${id}`,
      method: "DELETE",
    })
      .then((res) => console.log(res.data.msg))
      .catch((err) => console.log(err));
  };

  function confirmDelete() {
    if (window.confirm("Tem certeza que deseja excluir o item?") === true) {
      deleteItem(props.id);
      refreshPage();
    }
  }

  return (
    <div className="list-item-itens" key={props.id}>
      <button className="button-data">{props.desc}</button>
      <button className="buttons-list" onClick={() => navigate(`/editimovel/${props.id}`)}>
        <BsIcons.BsFillPencilFill size={12} />
      </button>
      <button className="buttons-list" onClick={() => confirmDelete()}>
        <BsIcons.BsFillTrashFill size={12} />
      </button>
    </div>
  );
};

export default ListItem;
