import React, { useState, useCallback, useEffect } from "react";
import "../../styles/FormProperty.scss";
import { api } from "../../../../services/api";

function FormProperty(props) {

  function refreshPage() {
    window.location.reload(true);
  }

  const listaEstados = [
    "Escolha um Estado",
    "GO",
    "SP",
    "DF",
    "MG",
    "MT",
    "RO",
    "AC",
    "AM",
    "RR",
    "PA",
    "AP",
    "TO",
    "MA",
    "PI",
    "CE",
    "RN",
    "PB",
    "PE",
    "AL",
    "SE",
    "BA",
    "ES",
    "RJ",
    "PR",
    "SC",
    "RS",
    "MS",
  ];

  const [descricao, setDescricao] = useState("");
  const [iptu, setIPTU] = useState("");
  const [nome_cartorio, setNomeCartorio] = useState("");
  const [registro, setRegistro] = useState("");
  const [medida, setMedida] = useState("");
  const [cep, setCEP] = useState("");
  const [rua, setRua] = useState("");
  const [cidade, setCidade] = useState("");
  const [uf, setUf] = useState("");
  const [bairro, setBairro] = useState("");
  const [adicional, setAdicional] = useState("");
  const [locador, setLocador] = useState("");

  const [responseData, setResponseData] = useState([]);

  function ChoiceItem(i) {
    return <option>{i}</option>;
  }

  function ChoiceItems(i) {
    return <option>{i.nome_locador}</option>;
  }

  async function onSubmit() {
    await api({
      method: "POST",
      url: "/imovel",
      data: {
        descricao: descricao,
        iptu: iptu,
        nome_cartorio: nome_cartorio,
        numero_registro: registro,
        medida: medida,
        cep: cep,
        rua: rua,
        cidade: cidade,
        uf: uf,
        bairro: bairro,
        adicional: adicional,
        locador_cadastrado: locador,
      },
    })
      .then((res) => {
        console.log(res.status);
        props.visible(false);
        // response = res.status
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const fetchData = useCallback(() => {
    api({
      method: "GET",
      url: "/get_locadores",
    })
      .then((response) => {
        setResponseData(response.data.entries);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="imovel">
      <form className="formulario_pro" onSubmit={(e) => e.preventDefault()}>
        <input
          className="input"
          placeholder="Descrição"
          onChange={(e) => setDescricao(e.target.value)}
        />
        <input
          className="input"
          placeholder="IPTU"
          onChange={(e) => setIPTU(e.target.value)}
        />
        <input
          className="input"
          placeholder="Nome do cartório"
          onChange={(e) => setNomeCartorio(e.target.value)}
        />
        <input
          className="input"
          placeholder="Número do resgistro"
          onChange={(e) => setRegistro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Medida"
          onChange={(e) => setMedida(e.target.value)}
        />
        <select className="input" onChange={(e) => setUf(e.target.value)}>
          {listaEstados.map(ChoiceItem)}
        </select>
        <input
          className="input"
          placeholder="CEP"
          onChange={(e) => setCEP(e.target.value)}
        />
        <input
          className="input"
          placeholder="Rua"
          onChange={(e) => setRua(e.target.value)}
        />
        <input
          className="input"
          placeholder="Cidade"
          onChange={(e) => setCidade(e.target.value)}
        />
        <input
          className="input"
          placeholder="Bairro"
          onChange={(e) => setBairro(e.target.value)}
        />
        <input
          className="input"
          placeholder="Adicional (Lote/Quadra/N°)"
          onChange={(e) => setAdicional(e.target.value)}
        />
        <select className="input" onChange={(e) => setLocador(e.target.value)}>
          <option>Escolha um locador</option>
          {responseData.map(ChoiceItems)}
        </select>

        <button className="botao_p" type="button" onClick={() => [onSubmit(), refreshPage()]}>
          Cadastrar
        </button>
      </form>
    </div>
  );
}

export default FormProperty;
