import { api } from "../../../../services/api";
import React, { useEffect, useState, useParams } from "react";
import Modal from "../../../../components/modal/Modal";
import ListItem from "../listItem/listItem";
import "./Paper.scss";

import FormualrioEdicaoImovel from "../../../editaveis/editarImovel"

function Paper() {
  // const { id } = useParams();
  const [viewEdit, setViewEdit] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    api.get("/get_imoveis").then((res) => {
      setData(res.data.entries);
    });
  }, []);

  return (
    <>
      {viewEdit ? (
        <Modal onClose={() => setViewEdit(false)}>
          <FormualrioEdicaoImovel visible={viewEdit} setVisible={setViewEdit} />
        </Modal>
      ) : null}
      <div className="paper">
        <div className="itens-lista">
          {data.map((item) => {
            return (
              <div key={item.imovel_id}>
                <ListItem
                  id={item.imovel_id}
                  desc={item.descricao}
                  toggleEdit={setViewEdit}
                ></ListItem>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}

export default Paper;
