import React, { useContext } from "react";
import "./Topbar.scss";
import * as BiIcons from "react-icons/bi";
import { AuthContext } from "../../contexts/Auth"

function Topbar() {
  const { logout } = useContext(AuthContext)
  const handleLogout = () => {
    logout();
  }
  return (
    <>
      <div className="top-bar">
        <div className="sair">
          {<BiIcons.BiExit size={25} onClick={handleLogout}/>}
        </div>
      </div>
    </>
  );
}

export default Topbar;