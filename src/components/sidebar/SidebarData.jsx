import React from "react";
import * as BsIcons from "react-icons/bs"
import * as MdIcons from "react-icons/md"
import * as FaIcons from "react-icons/fa"

export const SidebarData = [
  {
    name: "Inical",
    icon: <MdIcons.MdHome size={20} />,
    path: "/",  
  },
  {
    name: "Adicionar locador",
    icon: <MdIcons.MdPersonAddAlt1 size={20} />,
    path: "/locator",  
  },
  {
    name: "Adicionar Imóvel",
    icon: <MdIcons.MdAddBusiness size={20} />,
    path: "/property",
  },
  {
    name: "Adicionar contrato",
    icon: <MdIcons.MdNoteAdd size={20} />,
    path: "/agreement",
  },
  {
    name: "Parcelas",
    icon: <BsIcons.BsFillCalendarCheckFill size={20} />,
    path: "/installments",
  },
  {
    name: "Relatórios",
    icon: <BsIcons.BsFillFileEarmarkArrowDownFill size={20} />,
    path: "/reports",
  }
];
