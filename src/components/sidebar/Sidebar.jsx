import React, { useEffect, useState, useContext } from "react";
import { SidebarData } from "./SidebarData";
import { FaBars } from "react-icons/fa";
import { AiOutlineClose } from "react-icons/ai";
import { NavLink } from 'react-router-dom';
import "./Sidebar.scss";
import { AuthContext } from "../../contexts/Auth";

const Sidebar = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const { user } = useContext(AuthContext)

  return (
   <div className="container">
      <div style={{ width: isOpen ? "255px" : "70px" }} className="sidebar">
        <div className="top_section">
          <h1 style={{ display: isOpen ? "block" : "none" }} className="user">
          {user.email}
          </h1>
          <div
            style={{ marginLeft: isOpen ? "10%" : "0px" }}
            className="bars"
            onClick={toggle}
          >
            {!isOpen ? <FaBars /> : <AiOutlineClose />}
          </div>
        </div>
        {SidebarData.map((item, index) => (
          <NavLink
            to={item.path}
            key={index}
            className="link"
          >
            <div className="icon">{item.icon}</div>
            <div
              style={{ display: isOpen ? "block" : "none" }}
              className="link_text"
            >
              {item.name}
            </div>
          </NavLink>
        ))}
      </div>
      <main>{children}</main>
    </div>
  );
};

export default Sidebar;
