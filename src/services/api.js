import axios from "axios";

export const api = axios.create({
    baseURL: "http://192.168.20.244:5000"
});

export const createSession = async (email, password) => {
    return api.post("/auth/login", { email, password });
};