import React, { useContext } from "react";
import { Route, Routes, Navigate } from "react-router-dom";

import HomePage from "../pages/homepage/HomePage.jsx";
import Property from "../pages/property/Property.jsx";
import Reports from "../pages/reports/Reports.jsx";
import Agreement from "../pages/agreement/Agreement.jsx";
import Locator from "../pages/locator/locator.jsx";
import EditFormProperty from "../pages/editaveis/editarImovel.jsx";
import EditFormAgreement from "../pages/editaveis/editarContrato";
import EditFormLocator from "../pages/editaveis/editarLocador.jsx";
import Installments from "../pages/installments/installments.jsx";
import Login from "../pages/login/Login.jsx";

import { AuthContext } from "../contexts/Auth.jsx";

function AllRoutes() {
  const Private = ({ children }) => {
    const { authenticated, loading } = useContext(AuthContext);

    if (loading) {
      return <div className="loading">Carregando...</div>;
    }

    if (!authenticated) {
      return <Navigate to="/login" />;
    }
    return children;
  };
  return (
    <Routes>
      <Route path="login" element={<Login />} />
      <Route
        path="/"
        element={
          <Private>
            <HomePage />
          </Private>
        }
      />
      <Route
        path="locator"
        element={
          <Private>
            <Locator />
          </Private>
        }
      />
      <Route
        path="property"
        element={
          <Private>
            <Property />
          </Private>
        }
      />
      <Route
        path="agreement"
        element={
          <Private>
            <Agreement />
          </Private>
        }
      />
      <Route
        path="reports"
        element={
          <Private>
            <Reports />
          </Private>
        }
      />
      <Route
        path="installments"
        element={
          <Private>
            <Installments />
          </Private>
        }
      />
      <Route
        path="editimovel/:id"
        element={
          <Private>
            <EditFormProperty />
          </Private>
        }
      />
      <Route
        path="editcontrato/:id"
        element={
          <Private>
            <EditFormAgreement />
          </Private>
        }
      />
      <Route
        path="editlocador/:id"
        element={
          <Private>
            <EditFormLocator />
          </Private>
        }
      />
    </Routes>
  );
}

export default AllRoutes;
